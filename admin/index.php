<?php
session_start();

if (isset($_SESSION['username'])) {
  header("Location: plugins.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Plugins | Admin</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="../files/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body>
    <?php include("../files/navbar.php"); ?>
    <div class="container">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Admin Login</h3>
        </div>
        <div class="panel-body">
          <?php
            if (isset($_POST['submit'])) {
              if (isset($_POST['username']) && isset($_POST['password'])) {
    						$pass = $_POST['password'];
    						$user = $_POST['username'];
    						if ($user === "admin") {
    							if (password_verify($pass, '$2y$10$CtEyPlpktTsNQc79GRFzceab76VSfVMOpdjQU0xrPzLdIXcBL2ftm')) {
    								$_SESSION["username"] =  "admin";
                    header("Location: plugins.php");
    							} else {
    								?>
    								<p style="color: red;">Invalid password</p>
    								<?php
    							}
    						} else {
    							?>
    						<p style="color: red;">Invalid username</p>
    						<?php
    						}
    					} else {
    						?>
    						<p style="color: red;">You must provide a username and password</p>
    						<?php
    					}
            }
          ?>

          <div class="col-md-4"></div>
          <div class="col-md-4">
            <form action="index.php" method="POST">
    					Username
    					<br>
    					<input type="text" name="username" placeholder="Username">
    					<br>
    					<br>
    					Password
    					<br>
    					<input type="password" name="password" placeholder="Password">
    					<br>
    					<br>
    					<input type="submit" value="Submit" name="submit">
    				</form>
          </div>
        </div>
      </div>
    </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
