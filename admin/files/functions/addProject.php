<?php
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: ../index.php");
}
include("../../../files/connect.php");
include("../pluginTemplate.php");

function generateRandomString($length = 20) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getShortenedLink($url) {
  // Your ID and token
  $authToken = '8b03138736e79a524dc1b646a37182';

  // The data to send to the API
  $postData = array(
    'url' => $url,
    'is_secret' => true,
  );

  // Setup cURL
  $ch = curl_init('http://l.bumblebee.ml/api/v2/action/shorten?key=' . $authToken);
  curl_setopt_array($ch, array(
      CURLOPT_POST => TRUE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json'
      ),
      CURLOPT_POSTFIELDS => json_encode($postData)
  ));

  // Send the request
  $response = curl_exec($ch);

  // Check for errors
  if($response === FALSE){
      die(curl_error($ch));
  }

  return $response;
}


$uuid = generateRandomString();
$name = $_POST['name'];
$type = $_POST['type'];
$cost = $_POST['cost'];
$desc = $_POST['desc'];
$fd = $_POST['filedata'];
$file = basename($_FILES['file']['name']);

$hash = "null";
if ($_POST['password'] !== "") {
  $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
}

if (mysqli_num_rows(mysqli_query($con, "SELECT PluginName FROM Plugins WHERE PluginName='" . $name . "'")) > 1) {
  echo "Already exists in database!";
  die();
}

$dir = "../../../data/" . $uuid;
$tfile = $dir . "/" . basename($_FILES['file']['name']);
if (!is_dir($dir)) {
  if (!mkdir($dir)) {
    echo "Failed to make dir " . $dir;
    die();
  }
}

if (file_exists($tfile)) {
  echo "File already exists!";
  die();
}

if (move_uploaded_file($_FILES['file']['tmp_name'], $tfile)) {
} else {
  echo "Error while uploading!";
  die();
}

$shortLink = getShortenedLink("http://127.0.0.1/pluginsWebsite/getPlugin.php?uuid=" . $uuid);
$query = "INSERT INTO Plugins (PluginName,EncryptedName,FileName,Type,Description,Cost,ShareLink,Password)
  VALUES ('" . $name . "', '" . $uuid . "', '" . $file . "', '" . $type . "', '" . $desc . "', '" . $cost . "', '" . $shortLink . "', '" . $hash . "')";

if (!(mysqli_query($con, $query) === TRUE)) {
  echo "Error adding to database!";
  die();
}

insertData("../../../data/" . $uuid . "/index.php", "Plugins | " . $name, $fd, $name, $desc, $uuid);
header("Location: ../../plugins.php");
?>
