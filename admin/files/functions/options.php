<?php
define("host", "127.0.0.1");
define("user", "root");
define("password", "user");
define("database", "WebsiteData");
$con = mysqli_connect(host, user, password, database) or die("Error code: 1");

function rrmdir($dir) {
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (is_dir($dir."/".$object))
          rrmdir($dir."/".$object);
        else
          unlink($dir."/".$object);
      }
    }
    rmdir($dir);
  }
}

$dir = "";
function delete($id) {
  $con = mysqli_connect(host, user, password, database) or die("Error code: 1");
  $checkExists = mysqli_query($con, "SELECT ID,EncryptedName FROM Plugins WHERE ID=" . $id);

  $encr = mysqli_fetch_assoc($checkExists)['EncryptedName'];
  if (!(mysqli_num_rows($checkExists) > 0)) {
    echo "Doesn't exist";
    die();
  }

  mysqli_query($con, "DELETE FROM Plugins WHERE ID=" . $id);

  $dir = "../../../data/" . $encr . "/";
  rrmdir($dir);
}
header('Content-Type: application/json');

$aResult = array();
$id = 0;

if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }
if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }
if( !isset($aResult['error']) ) {
  switch($_POST['functionname']) {
     case 'delete':
        $aResult['result'] = "success ";
        if (!is_array($_POST['arguments']) || (count($_POST['arguments']) < 1)) {
            $aResult['error'] = 'Error in arguments!';
        } else {
            $aResult['result'] = $_POST['arguments'][0];
            $id = $_POST['arguments'][0];
        }
        break;
     default:
        $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
        break;
  }
}
echo json_encode($aResult);
delete($id);
?>
