<?php
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: ../index.php");
}
include("../../../files/connect.php");
include("../pluginTemplate.php");



$id = $_POST['id'];
$name = $_POST['name'];
$type = $_POST['type'];
$cost = $_POST['cost'];
$desc = $_POST['desc'];
$pass = $_POST['password'];
$fd = $_POST['filedata'];
$req = mysqli_query($con, "SELECT FileName,EncryptedName FROM Plugins WHERE ID='" . $id . "'");
$uuid = mysqli_fetch_assoc($req)['EncryptedName'];
$req = mysqli_query($con, "SELECT FileName,EncryptedName FROM Plugins WHERE ID='" . $id . "'");
$oldfile = mysqli_fetch_assoc($req)['FileName'];
$updateFile = ($_FILES['file']['size'] == 0) ? false : true;

if ($updateFile) {
  $file = basename($_FILES['file']['name']);
} else {
  $file = $oldfile;
}

if ($_POST['password'] === "") {
  $req = mysqli_query($con, "SELECT Password FROM Plugins WHERE ID='" . $id . "'");
  $pass = mysqli_fetch_assoc($req)['Password'];
} else {
  $pass = password_hash($_POST['password'], PASSWORD_DEFAULT);
}

if (!mysqli_num_rows(mysqli_query($con, "SELECT PluginName FROM Plugins WHERE PluginName='" . $name . "'")) > 1) {
  echo "Doesn't exists in database!";
  die();
}

$dir = "../../../data/" . $uuid;
$tfile = $dir . "/" . basename($_FILES['file']['name']);
$oldtFile = $dir . "/" . $oldfile;
if (!is_dir($dir)) {
  echo $dir . " is not a directory!";
  die();
}

if ($updateFile) {
  echo $tfile . " " . $oldtFile . " ";
  if (move_uploaded_file($_FILES['file']['tmp_name'], $tfile)) {
    if ($tfile !== $oldtFile) {
      if (is_writable($oldtFile)) {
        unlink($oldtFile);
      }
    }
  } else {
    echo "Error while uploading!";
    die();
  }
}

$query = "UPDATE Plugins SET PluginName='" . $name . "', FileName='" . $file . "', Type='" . $type . "',
    Description='" . $desc . "', Cost='" . $cost . "', Password='" . $pass . "' WHERE ID='" . $id . "'";

if (!(mysqli_query($con, $query) === TRUE)) {
  echo "Error adding to database!";
  die();
}

insertData("../../../data/" . $uuid . "/index.php", "Plugins | " . $name, $fd, $name, $desc, $uuid);
header("Location: ../../plugins.php");
?>
