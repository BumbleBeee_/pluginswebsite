<?php
session_start();
$pass = $_POST['data'];
$uuid = $_POST['uuid'];

if (isset($_SESSION['username'])) {
  echo "success";
  setSeshVerified($uuid);
  die();
}

include("../../files/connect.php");

$q = mysqli_query($con, "SELECT Password FROM Plugins WHERE EncryptedName='" . $uuid . "'");
$hash = mysqli_fetch_assoc($q)['Password'];

if (password_verify($pass, $hash)) {
  echo "success";
  setSeshVerified($uuid);
} else {
  echo "failed";
}

function setSeshVerified($id) {
  if (sizeof($_SESSION['verified']) > 0) {
    $d = $_SESSION['verified'];
    array_push($d, $id);
    $_SESSION['verified'] = $d;
  } else {
    $_SESSION['verified'] = array($id);
  }
}
?>
