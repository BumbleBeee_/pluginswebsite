<?php
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: ../index.php");
}

//TODO Password (auto gen)
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>BumbleBee | Admin</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="../files/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body>
    <?php
    function getData($encrypted) {
      $file = "../data/" . $encrypted . "/index.php";
      $fileContents = file_get_contents($file);
      $start = strpos($fileContents, "<!-- START -->")+14;
      $end = strpos($fileContents, "<!-- END -->");
      return substr($fileContents, $start, ($end-$start));
    }


    if (!($_SESSION['username'] === "admin")) {
      header("Location: ../index.php");
      die();
    }

    if (!isset($_GET['id'])) {
      echo "No data given!";
      die();
    }

    include("../files/connect.php");
    include("../files/navbar.php");

    $query = mysqli_query($con, "SELECT * FROM Plugins WHERE ID=" . $_GET['id']);
    $data = mysqli_fetch_assoc($query);
    if (!(mysqli_num_rows($query) > 0)) {
      echo "No data found!";
      die();
    }

    $name = $data['PluginName'];
    $type = $data['Type'];
    $cost = $data['Cost'];
    $desc = $data['Description'];
    $fd = $data['Description'];
    $file = $data['FileName'];
    $uuid = $data['EncryptedName'];
    $fd = getData($uuid);
    ?>
    <div class="container" style="padding-top: 10px;">
      <div class="col-md-2">
      </div>
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Editing plugin <?php echo $name; ?>
            </h3>
          </div>
          <div class="panel-body">
            <form action="files/functions/applyChanges.php" method="POST" enctype="multipart/form-data">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>Plugin Name</strong></span>
                <input value="<?php echo $name; ?>" type="text" name="name" class="form-control" placeholder="Plugin Name" aria-describedby="basic-addon1">
              </div>

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>Plugin Type</strong>&nbsp;</span>
                <select style="margin-left: 10px;" name="type">
                  <?php
                  if ($type === "public") {
                    echo "<option selected='true' value='public'>Public</option>";
                    echo "<option value='private'>Private</option>";
                  } else {
                    echo "<option value='public'>Public</option>";
                    echo "<option selected='true' value='private'>Private</option>";
                  }
                  ?>
                </select>
              </div>

              <div id="alert"></div>
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong><a href="#" style="text-decoration:none; color:black;" onclick="inputPassword();">&nbsp;&nbsp;Password&nbsp;&nbsp;</a></strong></span>
                <input type="password" name="password" class="form-control" placeholder="Leave blank for no password" aria-describedby="basic-addon1">
              </div>

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span>
                <input value=<?php echo $cost; ?> type="text" name="cost" class="form-control" placeholder="Cost" aria-describedby="basic-addon1">
              </div>

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>Description:</strong></span>
                <input value="<?php echo $desc; ?>" type="text" name="desc" class="form-control" placeholder="Quick Description" aria-describedby="basic-addon1">
              </div>

              <strong>Full Description:</strong><br>
              <textarea name="filedata" rows="15%" cols="82%"><?php echo $fd; ?></textarea>

              <br><br><br>
              <strong>Upload File:</strong>
              <input type="file" name="file">

              <br><br>
              <center><button type="submit" name="id" value="<?php echo $_GET['id'] ?>">Submit</button></center>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>

  <script type="text/javascript">
  function inputPassword() {
    var pass = generatePassword();
    document.getElementsByName('password')[0].value = pass;
    document.getElementById('alert').innerHTML = '<div class="alert alert-success" role="alert" style="color:black; font-size:14px;">'
      + '<strong>Generated Password: </strong> ' + pass
      + '</div>';
  }

  function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#&%$*@",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }
  </script>
</html>
