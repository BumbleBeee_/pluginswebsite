<?php
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: ../plugins.php");
}

$page = 1;
if (isset($_GET['page'])) {
  $page = $_GET['page'];
}
if ($page < 1) {
  $page = 1;
}

$bil = ($page-1)*15;
$til = $page*15;
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>BumbleBee | Admin</title>
    <script src="files/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="files/sweetalert.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </script>
    <link rel="stylesheet" href="../files/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body>
    <?php
    if (!($_SESSION['username'] === "admin")) {
      header("Location: ../plugins.php");
      die();
    }
    include("../files/connect.php");
    include("../files/navbar.php");
    $_SESSION['returnurl'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    ?>
    <div class="container" style="padding-top: 10px;">
      <div class="col-md-9">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              My Public Plugins
              <div style="padding-left: 77%;" class="btn-group" role="group">
                <a href="addPlugin.php">
                  <button type="button" class="btn btn-primary">Add A Project</button>
                </a>
              </div>
            </h3>
          </div>
          <div class="panel-body">
            <table width="100%" class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>Plugin</th>
                  <th>Type</th>
                  <th>Link</th>
                 </tr>
               </thead>
               <?php
                $data = mysqli_query($con, "SELECT * FROM Plugins LIMIT ". $bil. "," . $til);
                while ($row = mysqli_fetch_assoc($data)) {
                  $link = $row['ShareLink'];
                  $id = $row['ID'];
                  echo "<tr>
                  <td width='25%'>" . $row['PluginName'] . "</td>
                  <td width='25%'>" . $row['Type'] . "</td>
                  <td><a style='text-decoration: none;' href='../getPlugin.php?uuid=" . $row['EncryptedName'] . "'><span class='label label-warning'>" . $link . "</span></a></td>
                  <td class='view'>
                  <a href='../getPlugin.php?uuid=" . $row['EncryptedName'] . "'>
                    <img class='viewBtn' width=128 height=128 src='files/searchIcon.png'>
                  </a>
                  <a href='editProject.php?id=" . $row['ID'] . "'>
                      <img class='actionBtn' width=16 height=16 src='https://cdn4.iconfinder.com/data/icons/simplicio/128x128/file_edit.png'>
                    </a>
                    <a onclick='del(" . $row['ID'] . ");' href='#'>
                      <img class='actionBtn' width=16 height=16 src='https://www.vivus.es/assets/icons/do_not.svg'>
                    </a>
                  </td>
                  </tr>";
                }
               ?>
              </table>
              <nav aria-label="Page navigation">
                <ul class="pagination">
                  <?php
                  $pages = ceil(mysqli_num_rows(mysqli_query($con, "SELECT * FROM Plugins"))/15);
                  if ($page <= 1) {
                    echo '<li class="disabled">
                      <a aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>';
                  } else {
                    echo '<li>
                      <a href="plugins.php?page=' . ($page-1) . '" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>';
                  }

                  for ($i=1; $i <= $pages; $i++) {
                    if ($i == $page) {
                      echo '<li class="active"><a href="plugins.php?page=' . $id . '">' . $i . '</a></li>';
                    } else {
                      echo '<li><a href="plugins.php?page=' . $i . '">' . $i . '</a></li>';
                    }
                  }

                  if ($page+1 > $pages) {
                    echo '<li class="disabled">
                      <a aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>';
                  } else {
                    echo '<li>
                      <a href="plugins.php?page=' . ($page+1) . '" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>';
                  }
                  ?>
                </ul>
              </nav>
            </div>
          </div>
        </div>
  </body>


  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script type="text/javascript">
    function del(id) {
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this project!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
        swal("Deleted!", "Project has been successfully deleted.", "success");
        jQuery.ajax({
          type: "POST",
          url: 'files/functions/options.php',
          dataType: 'json',
          data: {functionname: 'delete', arguments: [id]},
          success: function (obj, textstatus) {
            if(!('error' in obj)) {
                r = obj.result;
                setTimeout(function(){
                  location.reload();
                },1000);
            } else {
                console.log(obj.error);
            }
          }
        });
      });
    }
  </script>
</html>
