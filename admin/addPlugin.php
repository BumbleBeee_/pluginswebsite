<?php
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: ../index.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>BumbleBee | Admin</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="../files/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body>
    <?php
    if (!($_SESSION['username'] === "admin")) {
      header("Location: ../index.php");
      die();
    }
    include("../files/connect.php");
    include("../files/navbar.php");
    ?>
    <div class="container" style="padding-top: 10px;">
      <div class="col-md-2">
      </div>
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Add A Plugin
            </h3>
          </div>
          <div class="panel-body">
            <form action="files/functions/addProject.php" method="POST" enctype="multipart/form-data">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>Plugin Name</strong></span>
                <input type="text" name="name" class="form-control" placeholder="Plugin Name" aria-describedby="basic-addon1">
              </div>

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>Plugin Type</strong>&nbsp;</span>
                <select style="margin-left: 10px;" name="type">
                  <option value="public">Public</option>
                  <option value="private">Private</option>
                </select>
              </div>

              <div id="alert"></div>
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong><a href="#" style="text-decoration:none; color:black;" onclick="inputPassword();">&nbsp;&nbsp;Password&nbsp;&nbsp;</a></strong></span>
                <input type="password" name="password" class="form-control" placeholder="Leave blank for no password" aria-describedby="basic-addon1">
              </div>

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span>
                <input type="text" name="cost" class="form-control" placeholder="Cost" aria-describedby="basic-addon1">
              </div>

              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><strong>Description:</strong></span>
                <input type="text" name="desc" class="form-control" placeholder="Quick Description" aria-describedby="basic-addon1">
              </div>

              <strong>Full Description:</strong><br>
              <textarea name="filedata" rows="15%" cols="82%"></textarea>

              <br><br><br>
              <strong>Upload File:</strong>
              <input type="file" name="file">

              <br><br>
              <center><input type="submit" name="submit" value="Submit"></center>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>

  <script type="text/javascript">
  function inputPassword() {
    var pass = generatePassword();
    document.getElementsByName('password')[0].value = pass;
    document.getElementById('alert').innerHTML = '<div class="alert alert-success" role="alert" style="color:black; font-size:14px;">'
      + '<strong>Generated Password: </strong> ' + pass
      + '</div>';
  }

  function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#&%$*@",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }
  </script>
</html>
