<?php
  session_start();
  $id = "aiHLN7F252zwoNJhEf7k";

  if (!isset($_SESSION["verified"])) {
    header("Location: ../../getPlugin.php?uuid=aiHLN7F252zwoNJhEf7k");
    die();
  }
  $d = $_SESSION["verified"];
  if (!in_array($id, $d)) {
    header("Location: ../../getPlugin.php?uuid=aiHLN7F252zwoNJhEf7k");
    die();
  }

  include("../../files/navbar.php");
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <title>Plugins | Dummy Plugin</title>
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <link rel="stylesheet" href="../../files/style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
      <div class="container" style="padding-top: 10px;">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                Dummy Plugin
                  <div style="float: right;" class="btn-group" role="group">
                    <a href="../../files/functions/download.php?name=aiHLN7F252zwoNJhEf7k">
                      <button type="button" class="btn btn-primary">Download</button>
                    </a>
                  </div>
                  <div style="color: grey; font-size: 95%;">This is a quick description of the plugin</div>
              </h3>
            </div>
            <div class="panel-body" style="word-wrap: break-word;">
            <!-- START -->
This is a long description of the plugin
            <!-- END -->
            </div>
          </div>
        </div>
      </div>
    </body>
  </html>