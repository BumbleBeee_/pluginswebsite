<?php
session_start();

function setSeshVerified($id) {
  if (sizeof($_SESSION['verified']) > 0) {
    $d = $_SESSION['verified'];
    array_push($d, $id);
    $_SESSION['verified'] = $d;
  } else {
    $_SESSION['verified'] = array($id);
  }
}

$uuid = $_GET['uuid'];
if (!isset($uuid)) {
  header("Location: index.php");
  die();
}
include("files/connect.php");

$q = mysqli_query($con, "SELECT EncryptedName,Password FROM Plugins WHERE EncryptedName='" . $uuid . "'");
$data = mysqli_fetch_assoc($q);
$encr = $data['EncryptedName'];
$pass = $data['Password'];
$url = "http://127.0.0.1/pluginsWebsite/data/" . $encr;

if ($pass === "null") {
  setSeshVerified($encr);
  header("Location: " . $url);
  die();
}

if (isset($_SESSION['username'])) {
  setSeshVerified($encr);
  header("Location: " . $url);
  die();
}
?>
<!DOCTYPE html>
<html>
<style>
  .container {
    margin-top: 225px;
  }
</style>

  <head>
    <meta charset="utf-8">
    <title>Authentication Required</title>
    <script src="files/sweetalert.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="files/sweetalert.css">
  </head>
  <body>
    <center>
      <div class="container" style="padding-top: 10px;">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                Login
              </h3>
            </div>
            <div class="panel-body">
              <form>
                <label>Password: </label>
                <input type="password" name="name" id="name"><br><br>
                <input type="submit" id="submit" value="View Plugin">
              </form>
              </div>
            </div>
          </div>
        </div>
    </center>
  </body>

  <script>
  $(document).ready(function () {
    $("#submit").on("click", function(e){
        e.preventDefault();
        var data_val = $("#name").val();
        var uuid_val = "<?php echo $uuid; ?>";
        $.post("admin/files/checker.php", {data:data_val, uuid:uuid_val}, function(data) {
          console.log(data);
            if (data == 'success') {
              window.location = "<?php echo $url; ?>";
              return false;
            } else {
              swal("Incorrect password", "If you have not received a password please contact BumbleBee", "error");
            }
        });
    });

  });
  </script>

</html>
