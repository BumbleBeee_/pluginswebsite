<?php

define("host", "127.0.0.1");
define("user", "root");
define("password", "user");
define("database", "WebsiteData");

$con = mysqli_connect(host, user, password, database) or die("Error code: 1");

$tableExists = mysqli_query($con, "SHOW TABLES LIKE 'Plugins'");;
if (mysqli_num_rows($tableExists) < 1) {
  $create = "CREATE TABLE Plugins (
    ID INTEGER(128) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    PluginName VARCHAR(128) NOT NULL,
    EncryptedName VARCHAR(128) NOT NULL,
    FileName VARCHAR(128) NOT NULL,
    Type VARCHAR(128) NOT NULL,
    Description VARCHAR(512),
    Cost VARCHAR(128),
    ShareLink VARCHAR(128),
    Password VARCHAR(128)
  )";

  if (mysqli_query($con, $create) === FALSE) {
    echo "Error code: 2";
  }
}

?>
