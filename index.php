<?php
include("files/connect.php");

$page = 1;
if (isset($_GET['page'])) {
  $page = $_GET['page'];
}
if ($page < 1) {
  $page = 1;
}

$bil = ($page-1)*15;
$til = $page*15;

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>BumbleBee | Plugins</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="files/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body>
    <?php include("files/navbar.php"); ?>
    <div class="container" style="padding-top: 10px;">
      <div class="col-md-9">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">My Public Plugins</h3>
          </div>
          <div class="panel-body">
            <table width="100%" class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>Plugin</th>
                  <th>Description</th>
                  <th></th>
                 </tr>
               </thead>
               <?php
               //TODO Limits
                $data = mysqli_query($con, "SELECT * FROM Plugins WHERE Type='Public' LIMIT "  . $bil . "," . $til);
                while ($row = mysqli_fetch_assoc($data)) {
                  echo "<tr>
                  <td>" . $row['PluginName'] . "</td>
                  <td>" . $row['Description'] . "</td>
                  <td class='view'><a href='data/" . $row['EncryptedName'] . "'>View/Download</a></td>
                  </tr>";
                }
               ?>
              </table>

              <nav aria-label="Page navigation">
                <ul class="pagination">
                  <?php
                  $pages = ceil(mysqli_num_rows(mysqli_query($con, "SELECT * FROM Plugins WHERE Type='Public'"))/15);
                  if ($page <= 1) {
                    echo '<li class="disabled">
                      <a aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>';
                  } else {
                    echo '<li>
                      <a href="index.php?page=' . ($page-1) . '" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>';
                  }

                  for ($i=1; $i <= $pages; $i++) {
                    if ($i == $page) {
                      echo '<li class="active"><a href="index.php?page=' . $i . '">' . $i . '</a></li>';
                    } else {
                      echo '<li><a href="index.php?page=' . $i . '">' . $i . '</a></li>';
                    }
                  }

                  if ($page+1 > $pages) {
                    echo '<li class="disabled">
                      <a aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>';
                  } else {
                    echo '<li>
                      <a href="index.php?page=' . ($page+1) . '" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>';
                  }
                  ?>
                </ul>
              </nav>
            </div>
          </div>
        </div>

      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body">
            Ads go here?
          </div>
        </div>
      </div>
    </div>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
